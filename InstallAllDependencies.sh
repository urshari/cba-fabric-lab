    
#!/bin/bash
#
# SPDX-License-Identifier: Apache-2.0
#


echo "   __ __ ___   ___ ";
echo "  / //_// _ ) / _ |";
echo " / ,<  / _  |/ __ |";
echo "/_/|_|/____//_/ |_|";
echo "                   ";

echo "   ____ __              __ ";
echo "  / __// /_ ___ _ ____ / /_";
echo " _\ \ / __// _ \`// __// __/";
echo "/___/ \__/ \_,_//_/   \__/ ";
echo "                           ";



installDocker(){
    echo "Installing Docker"
    echo "Updating package list"
    sudo apt update -y
    echo "Installing prerequisites"
    sudo apt install apt-transport-https ca-certificates curl software-properties-common build-essential -y
    echo "Installing Docker"
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    sudo apt update -y
    apt-cache policy docker-ce
    sudo apt install docker-ce -y
    echo "Adding user name to docker group"
    sudo usermod -aG docker ${USER}
    echo "Intsalling docker-compose"
    sudo apt install docker-compose -y
}

installNodeJs(){

        cd ~
        curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
        sleep 2
        echo "Installing nodejs version 16"
        sudo apt install -y nodejs         
        node -v
        type node

}

installDependencies(){
        
        installDocker
        sleep 1
        installNodeJs

        sleep 5
        echo "Installation Successfull"
        echo "   ____         __";
        echo "  / __/___  ___/ /";
        echo " / _/ / _ \/ _  / ";
        echo "/___//_//_/\_,_/  ";
        echo "                  ";
        echo "Reboot your machine to continue"
}

installDependencies $1
